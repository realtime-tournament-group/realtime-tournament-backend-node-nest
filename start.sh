#! /bin/bash
git clone https://gitlab.com/realtime-tournament-group/realtime-tournament-frontend
cd realtime-tournament-frontend
yarn && yarn build
mv ./dist/* ../client
cd ..
ls -l
rm -rf ./realtime-tournament-frontend

