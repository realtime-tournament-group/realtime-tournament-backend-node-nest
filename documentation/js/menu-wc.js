'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">nest-app documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-AppModule-8245d245f8899c554286b1eaf5025a2f"' : 'data-target="#xs-controllers-links-module-AppModule-8245d245f8899c554286b1eaf5025a2f"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AppModule-8245d245f8899c554286b1eaf5025a2f"' :
                                            'id="xs-controllers-links-module-AppModule-8245d245f8899c554286b1eaf5025a2f"' }>
                                            <li class="link">
                                                <a href="controllers/AppController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AppModule-8245d245f8899c554286b1eaf5025a2f"' : 'data-target="#xs-injectables-links-module-AppModule-8245d245f8899c554286b1eaf5025a2f"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AppModule-8245d245f8899c554286b1eaf5025a2f"' :
                                        'id="xs-injectables-links-module-AppModule-8245d245f8899c554286b1eaf5025a2f"' }>
                                        <li class="link">
                                            <a href="injectables/AppService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>AppService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/AuthModule.html" data-type="entity-link">AuthModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-AuthModule-b9aa3de5ccc2b8295f2c762ba5c98304"' : 'data-target="#xs-controllers-links-module-AuthModule-b9aa3de5ccc2b8295f2c762ba5c98304"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AuthModule-b9aa3de5ccc2b8295f2c762ba5c98304"' :
                                            'id="xs-controllers-links-module-AuthModule-b9aa3de5ccc2b8295f2c762ba5c98304"' }>
                                            <li class="link">
                                                <a href="controllers/AuthController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AuthController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AuthModule-b9aa3de5ccc2b8295f2c762ba5c98304"' : 'data-target="#xs-injectables-links-module-AuthModule-b9aa3de5ccc2b8295f2c762ba5c98304"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AuthModule-b9aa3de5ccc2b8295f2c762ba5c98304"' :
                                        'id="xs-injectables-links-module-AuthModule-b9aa3de5ccc2b8295f2c762ba5c98304"' }>
                                        <li class="link">
                                            <a href="injectables/AuthService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>AuthService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/JwtStrategy.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>JwtStrategy</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/GatewayModule.html" data-type="entity-link">GatewayModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-GatewayModule-dd20d4e6d9a4f403defdd0964c28059d"' : 'data-target="#xs-controllers-links-module-GatewayModule-dd20d4e6d9a4f403defdd0964c28059d"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-GatewayModule-dd20d4e6d9a4f403defdd0964c28059d"' :
                                            'id="xs-controllers-links-module-GatewayModule-dd20d4e6d9a4f403defdd0964c28059d"' }>
                                            <li class="link">
                                                <a href="controllers/TournamentGatewayController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TournamentGatewayController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-GatewayModule-dd20d4e6d9a4f403defdd0964c28059d"' : 'data-target="#xs-injectables-links-module-GatewayModule-dd20d4e6d9a4f403defdd0964c28059d"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-GatewayModule-dd20d4e6d9a4f403defdd0964c28059d"' :
                                        'id="xs-injectables-links-module-GatewayModule-dd20d4e6d9a4f403defdd0964c28059d"' }>
                                        <li class="link">
                                            <a href="injectables/TournamentGatewayService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>TournamentGatewayService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/JudgeModule.html" data-type="entity-link">JudgeModule</a>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-JudgeModule-04c125d69dbcaa9b6d9e9a955671d1d2"' : 'data-target="#xs-injectables-links-module-JudgeModule-04c125d69dbcaa9b6d9e9a955671d1d2"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-JudgeModule-04c125d69dbcaa9b6d9e9a955671d1d2"' :
                                        'id="xs-injectables-links-module-JudgeModule-04c125d69dbcaa9b6d9e9a955671d1d2"' }>
                                        <li class="link">
                                            <a href="injectables/JudgeService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>JudgeService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/MatchModule.html" data-type="entity-link">MatchModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-MatchModule-31ea1300004e4a00beea18c194296576"' : 'data-target="#xs-controllers-links-module-MatchModule-31ea1300004e4a00beea18c194296576"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-MatchModule-31ea1300004e4a00beea18c194296576"' :
                                            'id="xs-controllers-links-module-MatchModule-31ea1300004e4a00beea18c194296576"' }>
                                            <li class="link">
                                                <a href="controllers/MatchController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MatchController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-MatchModule-31ea1300004e4a00beea18c194296576"' : 'data-target="#xs-injectables-links-module-MatchModule-31ea1300004e4a00beea18c194296576"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-MatchModule-31ea1300004e4a00beea18c194296576"' :
                                        'id="xs-injectables-links-module-MatchModule-31ea1300004e4a00beea18c194296576"' }>
                                        <li class="link">
                                            <a href="injectables/MatchService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>MatchService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/PlayerModule.html" data-type="entity-link">PlayerModule</a>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-PlayerModule-a64544ccc497c5662d64898b4eea87ea"' : 'data-target="#xs-injectables-links-module-PlayerModule-a64544ccc497c5662d64898b4eea87ea"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-PlayerModule-a64544ccc497c5662d64898b4eea87ea"' :
                                        'id="xs-injectables-links-module-PlayerModule-a64544ccc497c5662d64898b4eea87ea"' }>
                                        <li class="link">
                                            <a href="injectables/PlayerService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>PlayerService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/TournamentModule.html" data-type="entity-link">TournamentModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-TournamentModule-26b59162f22492bda63fe35f98306d67"' : 'data-target="#xs-controllers-links-module-TournamentModule-26b59162f22492bda63fe35f98306d67"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-TournamentModule-26b59162f22492bda63fe35f98306d67"' :
                                            'id="xs-controllers-links-module-TournamentModule-26b59162f22492bda63fe35f98306d67"' }>
                                            <li class="link">
                                                <a href="controllers/TournamentController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TournamentController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-TournamentModule-26b59162f22492bda63fe35f98306d67"' : 'data-target="#xs-injectables-links-module-TournamentModule-26b59162f22492bda63fe35f98306d67"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-TournamentModule-26b59162f22492bda63fe35f98306d67"' :
                                        'id="xs-injectables-links-module-TournamentModule-26b59162f22492bda63fe35f98306d67"' }>
                                        <li class="link">
                                            <a href="injectables/TournamentService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>TournamentService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/UsersModule.html" data-type="entity-link">UsersModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-UsersModule-f835d5a85ceb77fada8b35d8186c3b81"' : 'data-target="#xs-controllers-links-module-UsersModule-f835d5a85ceb77fada8b35d8186c3b81"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-UsersModule-f835d5a85ceb77fada8b35d8186c3b81"' :
                                            'id="xs-controllers-links-module-UsersModule-f835d5a85ceb77fada8b35d8186c3b81"' }>
                                            <li class="link">
                                                <a href="controllers/UsersController.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UsersController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-UsersModule-f835d5a85ceb77fada8b35d8186c3b81"' : 'data-target="#xs-injectables-links-module-UsersModule-f835d5a85ceb77fada8b35d8186c3b81"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-UsersModule-f835d5a85ceb77fada8b35d8186c3b81"' :
                                        'id="xs-injectables-links-module-UsersModule-f835d5a85ceb77fada8b35d8186c3b81"' }>
                                        <li class="link">
                                            <a href="injectables/UsersService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>UsersService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/Gift.html" data-type="entity-link">Gift</a>
                            </li>
                            <li class="link">
                                <a href="classes/Judge.html" data-type="entity-link">Judge</a>
                            </li>
                            <li class="link">
                                <a href="classes/JudgeDataDto.html" data-type="entity-link">JudgeDataDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/Match.html" data-type="entity-link">Match</a>
                            </li>
                            <li class="link">
                                <a href="classes/matchCascade1568457195727.html" data-type="entity-link">matchCascade1568457195727</a>
                            </li>
                            <li class="link">
                                <a href="classes/matchCs1568458468386.html" data-type="entity-link">matchCs1568458468386</a>
                            </li>
                            <li class="link">
                                <a href="classes/matchJoin1568456558314.html" data-type="entity-link">matchJoin1568456558314</a>
                            </li>
                            <li class="link">
                                <a href="classes/matchJoinless1568456721768.html" data-type="entity-link">matchJoinless1568456721768</a>
                            </li>
                            <li class="link">
                                <a href="classes/matchLes1568457132217.html" data-type="entity-link">matchLes1568457132217</a>
                            </li>
                            <li class="link">
                                <a href="classes/matchNoncad1568473761834.html" data-type="entity-link">matchNoncad1568473761834</a>
                            </li>
                            <li class="link">
                                <a href="classes/matchNoncascade1568459101574.html" data-type="entity-link">matchNoncascade1568459101574</a>
                            </li>
                            <li class="link">
                                <a href="classes/Player.html" data-type="entity-link">Player</a>
                            </li>
                            <li class="link">
                                <a href="classes/PlayerActiveDto.html" data-type="entity-link">PlayerActiveDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/PlayerDataDto.html" data-type="entity-link">PlayerDataDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/PrivateDataDto.html" data-type="entity-link">PrivateDataDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/SetJudgeDto.html" data-type="entity-link">SetJudgeDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/Tournament.html" data-type="entity-link">Tournament</a>
                            </li>
                            <li class="link">
                                <a href="classes/TournamentGateway.html" data-type="entity-link">TournamentGateway</a>
                            </li>
                            <li class="link">
                                <a href="classes/User.html" data-type="entity-link">User</a>
                            </li>
                            <li class="link">
                                <a href="classes/userDX1568402306155.html" data-type="entity-link">userDX1568402306155</a>
                            </li>
                            <li class="link">
                                <a href="classes/userFlow1568391941923.html" data-type="entity-link">userFlow1568391941923</a>
                            </li>
                            <li class="link">
                                <a href="classes/userInitialize1568395134802.html" data-type="entity-link">userInitialize1568395134802</a>
                            </li>
                            <li class="link">
                                <a href="classes/userInitializf1568395652910.html" data-type="entity-link">userInitializf1568395652910</a>
                            </li>
                            <li class="link">
                                <a href="classes/userInitializfcx1568401165291.html" data-type="entity-link">userInitializfcx1568401165291</a>
                            </li>
                            <li class="link">
                                <a href="classes/userQ1568401998681.html" data-type="entity-link">userQ1568401998681</a>
                            </li>
                            <li class="link">
                                <a href="classes/userReq1568401263181.html" data-type="entity-link">userReq1568401263181</a>
                            </li>
                            <li class="link">
                                <a href="classes/userReqof1568401313580.html" data-type="entity-link">userReqof1568401313580</a>
                            </li>
                            <li class="link">
                                <a href="classes/userRes1568401239908.html" data-type="entity-link">userRes1568401239908</a>
                            </li>
                            <li class="link">
                                <a href="classes/userRoundNumber1568453582015.html" data-type="entity-link">userRoundNumber1568453582015</a>
                            </li>
                            <li class="link">
                                <a href="classes/WebsocketRoomGateway.html" data-type="entity-link">WebsocketRoomGateway</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/DtoTournament.html" data-type="entity-link">DtoTournament</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/PlayerPayload.html" data-type="entity-link">PlayerPayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/PrivateData.html" data-type="entity-link">PrivateData</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/PrivateDataDto.html" data-type="entity-link">PrivateDataDto</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/PublicTournamentDto.html" data-type="entity-link">PublicTournamentDto</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});