FROM node:10.22
WORKDIR /usr/src/app
COPY . ./
RUN chmod +x ./start.sh && ./start.sh /bin/bash
RUN yarn 
RUN yarn typeorm migration:run
RUN npm i -g ts-node
EXPOSE 4000
EXPOSE 4500
EXPOSE 4600

