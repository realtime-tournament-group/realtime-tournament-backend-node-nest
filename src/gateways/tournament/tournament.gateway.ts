import {
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServerOptions,
  WebSocketServer,
  OnGatewayConnection,
  OnGatewayDisconnect,
} from '@nestjs/websockets';
import { Socket } from 'socket.io';

interface PrivateData {
  privateHash: string;
}
interface PlayerPayload {
  userName: string;
  hash: string;
}

@WebSocketGateway(4500, { namespace: '/tournament' })
export class TournamentGateway
  implements OnGatewayConnection, OnGatewayDisconnect {
  handleConnection(client: Socket) {
    client.emit('user_connected', 'success connection tournament');
  }
  handleDisconnect(client: Socket) {
    client.leaveAll();
  }
  @SubscribeMessage('connect_room')
  handleJoinRoom(client: Socket, payload: string) {
    console.log('--------------------------------------');
    client.join(payload);
    client.emit('user_connected', 'user success connected');
  }

  @WebSocketServer() server;
}
