import {
  Controller,
  Get,
  Post,
  Req,
  Res,
  Param,
  Body,
  Put,
  Delete,
} from '@nestjs/common';
import { TournamentService } from '../../tournament/tournament.service';
import { Tournament } from '../../entities/tournament.entity';
import { Player } from '../../entities/player.entity';
import { PlayerDataDto } from '../../tournament/dto/player.data';
import { JudgeDataDto } from '../../tournament/dto/judge.data';
import { TournamentGatewayService } from './tournament.gateway.service';
import { SetJudgeDto } from '../../tournament/dto/SetJudgeDto';
import { PlayerActiveDto } from '../../tournament/dto/PlayerActiveDto';

interface DtoTournament {
  tournament: Tournament;
}

interface PrivateDataDto {
  privateHash: string;
}

@Controller('tournament/realtime')
export class TournamentGatewayController {
  constructor(private readonly gateTournament: TournamentGatewayService) {}

  @Post('/start')
  async startTournament(@Body() payload: PrivateDataDto) {
    console.log(payload);
    console.log('started_tournament');
    return this.gateTournament.start(payload);
  }
  @Put('/stop')
  async stopTournament(@Body() payload: PrivateDataDto) {
    console.log(payload);

    return this.gateTournament.stop(payload);
  }
  @Post('queue/join')
  async joinQueue(@Body() payload: PlayerActiveDto) {
    console.log(payload);

    return this.gateTournament.joinQueue(payload);
  }
  @Post('queue/leave')
  async leaveQueue(@Body() payload: PlayerActiveDto) {
    console.log('leave');

    return this.gateTournament.leaveQueue(payload);
  }
  @Post('connect/user')
  async connectUser(@Body() payload: PlayerDataDto) {
    return this.gateTournament.playerConnect(payload);
  }
  @Post('connect/check')
  async connectCheck(@Body()
  payload: {
    hash: string;
    tournamentName: string;
  }) {
    return this.gateTournament.playerCheck(payload);
  }
  @Post('match/apply')
  async applyMatch(@Body() payload: PlayerDataDto) {
    console.log(payload);

    return this.gateTournament.applyMatch(payload);
  }
  @Post('match/cancel')
  async cancelMatch(@Body() payload: PlayerDataDto) {
    console.log(payload);

    return this.gateTournament.cancelMatch(payload);
  }
  @Post('/judge')
  async addJudge(@Body() payload: SetJudgeDto) {
    console.log(payload);

    return this.gateTournament.setJudge(payload);
  }
  @Post('/judge/add')
  async removeJudge(@Body() payload: SetJudgeDto) {
    console.log(payload);

    return this.gateTournament.removeJudge(payload);
  }
  @Post('match/set_score')
  async setScore(@Body() payload: JudgeDataDto) {
    console.log(payload);

    return this.gateTournament.setScore(payload);
  }
}
