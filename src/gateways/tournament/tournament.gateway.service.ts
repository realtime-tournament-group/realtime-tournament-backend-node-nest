import { Injectable, BadRequestException, RequestMethod } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { sha256 } from "js-sha256";
import { Tournament } from "../../entities/tournament.entity";
import { Repository } from "typeorm";
import { tournamentConfig } from "../../tournament/contants";
import { Player } from "../../entities/player.entity";
import { TournamentGateway } from "./tournament.gateway";
import { PrivateDataDto } from "../../tournament/dto/private.data";
import { SetJudgeDto } from "../../tournament/dto/SetJudgeDto";
import { TournamentService } from "../../tournament/tournament.service";
import { UsersService } from "../../users/users.service";
import { PlayerDataDto } from "../../tournament/dto/player.data";
import { JudgeDataDto } from "../../tournament/dto/judge.data";
import { JudgeService } from "../../judge/judge.service";
import { PlayerService } from "../../player/player.service";
import { PlayerActiveDto } from "../../tournament/dto/PlayerActiveDto";
import { MatchService } from "../../match/match.service";

@Injectable()
export class TournamentGatewayService {
  constructor(
    private readonly tourService: TournamentService,
    private readonly wssTournament: TournamentGateway,
    private readonly playerService: PlayerService,
    private readonly judgeService: JudgeService,
    private readonly usersService: UsersService,
    private readonly matchService: MatchService
  ) {}
  async start(payload: PrivateDataDto) {
    const model = await this.tourService.findByPrivateHash(payload.privateHash);
    if (model) {
      model.status = true;
      await this.tourService.update(model);
      console.log(model);
      this.wssTournament.server
        .to(model.name)
        .emit("update_tournament", { status: true });
    } else {
      return "tournament is not found";
    }
  }
  async stop(payload: PrivateDataDto) {
    const model = await this.tourService.findByPrivateHash(payload.privateHash);
    if (model) {
      model.status = false;
      console.log(model);

      this.wssTournament.server
        .to(model.name)
        .emit("update_tournament", "status");
    }
  }
  async playerConnect(payload: PlayerDataDto) {
    const model = await this.tourService.findByName(payload.tournamentName);
    if (model) {
      console.log(payload);
      let user = await this.usersService.findUserByName(payload.name);
      if (!user) {
        user = await this.usersService.createUser(
          payload.name,
          sha256(payload.name)
        );
      }
      const alwaysPlayer = await model.players.find(
        item => item.name === payload.name
      );
      if (alwaysPlayer) {
        return { status: alwaysPlayer.hash };
      } else {
        const player = await this.playerService.createFromUser(user);
        console.log("cus");
        model.players.push(player);
        player.tournament = model;
        await this.playerService.updatePlayer(player);
        await this.tourService.update(model);
        this.wssTournament.server.to(model.name).emit("player_joined", {
          id: player.id,
          name: player.name,
          score: player.score
        });
        return { status: player.hash };
      }
    }
    return { status: false };
  }
  async playerCheck(payload: { hash: string; tournamentName: string }) {
    const model = await this.tourService.findByName(payload.tournamentName);
    console.log(model.players);
    if (model) {
      console.log(model.players);
      const user = model.players.find(player => player.hash === payload.hash);
      if (user) {
        return { status: true };
      }
    }
    return { status: false };
  }
  async joinQueue(payload: PlayerActiveDto) {
    const model = await this.tourService.findByName(payload.tournamentName);
    if (model) {
      const player = model.players.find(player => player.hash === payload.hash);

      if (!player.inqQueue) {
        player.inqQueue = true;
        const updateEntity = await this.playerService.updatePlayer(player);
        await this.tourService.update(model);
        console.log(updateEntity);
        this.wssTournament.server
          .to(model.name)
          .emit("user_joined_queue", player);
        this.generateMatch(model);
        return { hash: player.hash };
      }
    }
  }
  async generateMatch(tournament: Tournament) {
    const model = await this.tourService.findByName(tournament.name);
    console.log(model.players);
    const players = model.players.filter(player => player.inqQueue);
    const groupByRoundes = [
      { round: 1, players: [] },
      { round: 2, players: [] },
      { round: 3, players: [] }
    ];
    console.log("stage-1-ok");

    players.forEach(player => {
      const round = groupByRoundes.find(item => item.round === player.round);
      round.players.push(player);
      console.log(round);
    });
    groupByRoundes.forEach(async item => {
      const playersCount = item.players.length;
      for (let i = 0; i < playersCount; i += 2) {
        const match = await this.matchService.create(item.round);

        match.players = item.players.splice(i, 2);
        item.players.splice(i, 2).forEach((el: Player) => {
          el.matches.push(match);
          this.playerService.updatePlayer(el);
        });
        match.tournament = tournament;
        const updated = await this.matchService.update(match);
        item.players.forEach((user: Player) => {
          this.wssTournament.server
            .to(user.hash)
            .emit("user_update_match", match);
        });
        model.matches.push(updated);
      }
    });

    await this.tourService.update(tournament);
  }

  async leaveQueue(payload: PlayerActiveDto) {
    const model = await this.tourService.findByName(payload.tournamentName);

    const player = model.players.find(el => el.hash === payload.hash);
    console.log(model);
    if (player && player.inqQueue) {
      player.inGame = false;
      player.inqQueue = false;
      console.log(player);

      const updateEntity = await this.playerService.updatePlayer(player);
      this.tourService.update(model);
      console.log(updateEntity);
      this.wssTournament.server
        .to(model.name)
        .emit("user_leaved_queue", player);
      return "ok";
    }
  }
  async applyMatch(payload: PlayerDataDto) {
    const model = await this.tourService.findByName(payload.tournamentName);

    const player = model.players.find(el => el.name === payload.name);
    if (player && !player.inGame && player.inqQueue) {
      player.inGame = true;
      player.inqQueue = false;
      this.playerService.updatePlayer(player);
      console.log(model);

      this.wssTournament.server
        .to(model.name)
        .emit("user_applyed_match", player.name);
    }
  }
  async cancelMatch(payload: PlayerDataDto) {
    const model = await this.tourService.findByName(payload.tournamentName);

    const player = model.players.find(el => el.id === el.id);
    if (player && !player.inGame && player.inqQueue) {
      player.inGame = false;
      player.inqQueue = true;
      this.playerService.updatePlayer(player);
      console.log(model);

      this.wssTournament.server
        .to(model.name)
        .emit("user_cancel_match", player.name);
    }
  }
  async setJudge(payload: SetJudgeDto) {
    const model = await this.tourService.findByPrivateHash(payload.privateHash);
    const player = payload.player;
    if (model) {
      const player = model.players.find(el => el.id === player.id);
      const judge = this.judgeService.findInTournament(model, player);

      if (judge) {
        this.judgeService.add(model, player);
        const judge = await this.judgeService.findInTournament(model, player);
        console.log(model);

        this.wssTournament.server
          .to(model.name)
          .emit("admin_set_judge", { player: judge.player, id: judge.id });
      }
      // TODO: добавить сервис для добавления судей
    }
  }
  async removeJudge(payload: SetJudgeDto) {
    const model = await this.tourService.findByPrivateHash(payload.privateHash);
    const player = payload.player;
    if (model) {
      const player = model.players.find(el => el.id === player.id);
      const judge = await this.judgeService.findInTournament(model, player);

      if (judge) {
        this.judgeService.remove(model, player);
        console.log(model);

        this.wssTournament.server
          .to(model.name)
          .emit("admin_remove_judge", judge.player.name);
      }
      // TODO: добавить сервис для добавления судей
    }
  }
  async setScore(payload: JudgeDataDto) {
    const tournament = await this.tourService.findByPrivateHash(payload.hash);
    if (tournament) {
      const player = tournament.players.find(
        item => item.id === payload.player.id
      );

      if (player) {
        player.score++;
        if (player.score > 5) {
          player.round++;
          player.score = 0;
          player.inGame = false;
          player.inqQueue = false;
        }
        await this.playerService.updatePlayer(player);
        this.wssTournament.server.to(tournament.name).emit("user_add_score", {
          id: player.id,
          name: player.name,
          score: payload.score
        });
        return "ok";
      }
    }
  }
}
