import {
  SubscribeMessage,
  WebSocketServer,
  WebSocketGateway,
  OnGatewayInit,
  OnGatewayConnection,
  OnGatewayDisconnect
} from "@nestjs/websockets";
import { Socket, Server } from "socket.io";

@WebSocketGateway(4600, { namespace: "/chat" })
export class WebsocketRoomGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  handleConnection(client: any) {
    console.log("connected");
  }
  handleDisconnect(client: any) {
    console.log("disconnected");
  }
  afterInit(server: any) {
    console.log("initialized");
  }
  @WebSocketServer() server;

  @SubscribeMessage("connect_room")
  handleConnectRoom(client: Socket, name: string) {
    client.join(name);
  }

  @SubscribeMessage("message_to_server")
  handleMessage(client: Socket, payload: { name: string; message: string }) {
    console.log(payload.message);
    return this.server
      .to(Object.entries(client.rooms)[1][0])
      .emit("message_receive", payload);
  }
}
