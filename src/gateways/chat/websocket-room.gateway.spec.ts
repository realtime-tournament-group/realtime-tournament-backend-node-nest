import { Test, TestingModule } from "@nestjs/testing";
import { WebsocketRoomGateway } from "../websocket-room.gateway";

describe("WebsocketRoomGateway", () => {
  let gateway: WebsocketRoomGateway;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [WebsocketRoomGateway]
    }).compile();

    gateway = module.get<WebsocketRoomGateway>(WebsocketRoomGateway);
  });

  it("should be defined", () => {
    expect(gateway).toBeDefined();
  });
});
