import { Module } from "@nestjs/common";
import { TournamentModule } from "../tournament/tournament.module";
import { UsersModule } from "../users/users.module";
import { TournamentGateway } from "./tournament/tournament.gateway";
import { WebsocketRoomGateway } from "./chat/websocket-room.gateway";
import { TournamentGatewayService } from "./tournament/tournament.gateway.service";
import { TournamentGatewayController } from "./tournament/tournament.gateway.controller";
import { JudgeModule } from "../judge/judge.module";
import { PlayerModule } from "../player/player.module";
import { MatchModule } from "../match/match.module";

@Module({
  imports: [
    UsersModule,
    TournamentModule,
    JudgeModule,
    PlayerModule,
    MatchModule
  ],
  controllers: [TournamentGatewayController],
  providers: [
    WebsocketRoomGateway,
    TournamentGateway,
    TournamentGatewayService
  ],
  exports: [TournamentGatewayService]
})
export class GatewayModule {}
