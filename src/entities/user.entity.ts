import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  JoinTable,
  OneToMany,
  OneToOne,
  JoinColumn,
} from 'typeorm';
import { Tournament } from './tournament.entity';
import { Injectable } from '@nestjs/common';
import { Gift } from './gift.entity';
import { Match } from './match.entity';
import { Player } from './player.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column()
  password: string;

  @OneToOne(type => Player, player => player.user)
  @JoinColumn()
  player: Player;
}
