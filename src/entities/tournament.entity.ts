import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  JoinTable,
  OneToMany,
  OneToOne,
  JoinColumn,
  BeforeInsert,
} from 'typeorm';
import { Injectable } from '@nestjs/common';
import { Player } from './player.entity';
import { Gift } from './gift.entity';
import { Judge } from './judge.entity';
import { Match } from './match.entity';

@Entity()
export class Tournament {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column()
  privateHash: string;
  @Column()
  publicHash: string;
  @Column()
  status: boolean;
  @Column()
  open: boolean;
  @OneToMany(type => Match, match => match.tournament, {
    cascade: true,
    eager: true,
  })
  matches: Match[];

  @OneToMany(type => Player, player => player.tournament, {
    cascade: true,
    eager: true,
  })
  players: Player[];

  @Column({ nullable: true })
  creationDate: Date;

  @OneToMany(type => Gift, gift => gift.tournament)
  gifts: Gift[];
  @OneToMany(type => Judge, gift => gift.tournament)
  judges: Judge[];

  @BeforeInsert()
  updateDates() {
    this.creationDate = new Date();
  }
}
