import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  JoinTable,
  OneToMany,
  OneToOne,
  JoinColumn
} from "typeorm";
import { Injectable } from "@nestjs/common";
import { User } from "./user.entity";
import { Player } from "./player.entity";
import { Gift } from "./gift.entity";
import { Chat } from "./chat.entity";

@Entity()
export class ChatMessage {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  author: string;
  @Column({ length: 500 })
  message: string;

  @OneToOne(type => Chat)
  @JoinColumn()
  chat: Chat;
}
