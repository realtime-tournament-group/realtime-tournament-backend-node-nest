import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { Tournament } from './tournament.entity';
import { Injectable } from '@nestjs/common';
import { Player } from './player.entity';

@Entity()
export class Gift {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;
  @ManyToOne(type => Tournament, tournament => tournament.gifts)
  tournament: Tournament;

  @Column()
  hash: string;
  @ManyToOne(type => Player, user => user.gifts)
  player: Player;
}
