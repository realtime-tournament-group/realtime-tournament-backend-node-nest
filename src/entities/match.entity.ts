import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  JoinTable,
  OneToOne,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { Injectable } from '@nestjs/common';
import { Tournament } from './tournament.entity';
import { Gift } from './gift.entity';
import { Player } from './player.entity';

@Entity()
export class Match {
  @PrimaryGeneratedColumn()
  id: number;
  matchId: number;
  @ManyToMany(type => Player, player => player.matches, {
    cascade: true,
    eager: true,
  })
  @JoinTable()
  players: Player[];
  @Column()
  roundNumber: number;

  @Column()
  status: boolean;
  @ManyToOne(type => Tournament, tournament => tournament.matches)
  tournament: Tournament;
}
