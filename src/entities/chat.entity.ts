import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  JoinTable,
  OneToMany,
  OneToOne
} from "typeorm";
import { Injectable } from "@nestjs/common";
import { User } from "./user.entity";
import { Player } from "./player.entity";
import { Gift } from "./gift.entity";
import { Tournament } from "./tournament.entity";
import { ChatMessage } from "./chat.message.entity";

@Entity()
export class Chat {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @OneToOne(type => Tournament)
  tournament: Tournament;

  @OneToMany(type => ChatMessage, message => message.chat, {
    eager: true
  })
  messages: Promise<ChatMessage[]>;
}
