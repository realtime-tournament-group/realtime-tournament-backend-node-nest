import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  JoinTable,
  OneToOne,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { Injectable } from '@nestjs/common';
import { Tournament } from './tournament.entity';
import { Gift } from './gift.entity';
import { Player } from './player.entity';

@Entity()
export class Judge {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  hash: string;
  @OneToOne(type => Player)
  player: Player;
  @ManyToOne(type => Tournament, tournament => tournament.judges)
  tournament: Tournament;
}
