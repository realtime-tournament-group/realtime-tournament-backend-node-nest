import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  JoinTable,
  OneToMany,
  OneToOne,
  JoinColumn,
  ManyToOne,
} from 'typeorm';
import { Tournament } from './tournament.entity';
import { Injectable } from '@nestjs/common';
import { Gift } from './gift.entity';
import { Match } from './match.entity';
import { User } from './user.entity';

@Entity()
export class Player {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ unique: true })
  name: string;
  @Column()
  hash: string;

  @ManyToMany(type => Gift, gift => gift.player)
  @JoinTable()
  gifts: string;
  @ManyToOne(type => Tournament, tournament => tournament.players)
  tournament: Tournament;

  @OneToOne(type => User, user => user.player)
  @JoinColumn()
  user: User;

  @ManyToMany(type => Match, match => match.players)
  matches: Match[];

  @Column({ default: 0 })
  score: number;
  @Column({ default: false })
  inqQueue: boolean;
  @Column({ default: false })
  inGame: boolean;
  @Column({ default: 1 })
  round: number;
}
