import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Player } from '../entities/player.entity';
import { sha256 } from 'js-sha256';
import { Repository } from 'typeorm';
import { User } from '../entities/user.entity';

@Injectable()
export class PlayerService {
  constructor(
    @InjectRepository(Player)
    private readonly playerRepository: Repository<Player>,
  ) {}

  public async createPlayerWithHash(user: User) {
    const hash = sha256(user.name + new Date().toISOString());
    const player = await this.playerRepository.create({
      user,
      name: user.name,
      hash,
    });
    await this.playerRepository.save(player);
    return hash;
  }
  public async createFromUser(user: User): Promise<Player> {
    const hash = sha256(user.name + new Date().toISOString());
    const player = await this.playerRepository.create({
      user,
      name: user.name,
      hash,
    });
    await this.playerRepository.save(player);
    return this.findByUser(user);
  }
  public async findByUser(user: User): Promise<Player> {
    return this.playerRepository.findOne(
      { ...user },
      { relations: ['tournament'] },
    );
  }
  public async updatePlayer(player: Partial<Player>) {
    await this.playerRepository.save(player);
    return this.playerRepository.findOne(player.id);
  }
  public async findByhash(hash: string): Promise<Player> {
    return this.playerRepository.findOne(
      { hash },
      { relations: ['tournament'] },
    );
  }
}
