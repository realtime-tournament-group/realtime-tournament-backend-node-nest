import { Injectable } from "@nestjs/common";
import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";
import { Match } from "../entities/match.entity";
import { sha256 } from "js-sha256";
import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";

@Injectable()
export class MatchService {
  constructor(
    @InjectRepository(Match) private readonly matchRep: Repository<Match>
  ) {}
  async create(roundNumber: number = 1) {
    const model = await this.matchRep.create({ roundNumber, status: false });
    this.matchRep.save(model);
    return this.matchRep.findOne(model, { relations: ["players"] });
  }
  async update(match: Partial<Match>) {
    await this.matchRep.save(match);
    return this.matchRep.findOne(match, {
      relations: ["players", "tournament"]
    });
  }
  async find(id: number) {
    return this.matchRep.findOne({ id }, { relations: ["players"] });
  }
  async remove(match: Partial<Match>) {
    this.matchRep.delete(match);
  }
}
