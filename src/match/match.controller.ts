import { Controller, Get, Query, Param, Body } from "@nestjs/common";
import { MatchService } from "./match.service";


@Controller("match")
export class MatchController {
  constructor(private readonly matchService: MatchService) {}
  @Get(":id")
  async find(@Param("id") id: number) {
    return this.matchService.find(id);
  }
}
