import { Module } from "@nestjs/common";
import { MatchService } from "./match.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Match } from "../entities/match.entity";
import { MatchController } from "./match.controller";
import { PlayerModule } from "../player/player.module";

@Module({
  imports: [TypeOrmModule.forFeature([Match]), PlayerModule],
  providers: [MatchService],
  exports: [MatchService],
  controllers: [MatchController]
})
export class MatchModule {}
