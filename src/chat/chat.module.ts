import { Module } from "@nestjs/common";
import { ChatService } from "./chat.service";
import { ChatController } from "./chat.controller";
import { TypeOrmModule } from "@nestjs/typeorm";
import { User } from "../entities/user.entity";
import { Chat } from "../entities/chat.entity";
import { ChatMessage } from "../entities/chat.message.entity";

@Module({
  imports: [TypeOrmModule.forFeature([Chat, ChatMessage])],
  providers: [ChatService],
  controllers: [ChatController],
  exports: [ChatService]
})
export class ChatModule {}
