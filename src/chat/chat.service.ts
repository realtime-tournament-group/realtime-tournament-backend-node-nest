import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { User } from "../entities/user.entity";
import { Repository } from "typeorm";
import { Chat } from "../entities/chat.entity";
import { ChatMessage } from "../entities/chat.message.entity";
import { Tournament } from "../entities/tournament.entity";
import { Player } from "../entities/player.entity";

@Injectable()
export class ChatService {
  constructor(
    @InjectRepository(Chat)
    private readonly chatRepository: Repository<Chat>,
    @InjectRepository(ChatMessage)
    private readonly messageRepository: Repository<ChatMessage>
  ) {}

  async createChat(name: string, tournament: Tournament) {
    return this.chatRepository.insert({ name, tournament });
  }

  async findChatById(id: number) {
    return this.chatRepository.findOne(id);
  }

  async sendMessage(
    message: string,
    chat: Partial<Chat>,
    player: Partial<Player>
  ) {
    const chatEntity = await this.findChatById(chat.id);
    return this.messageRepository.insert({
      message,
      chat: chatEntity,
      author: player.name
    });
  }

  async getMessages(chat: Partial<Chat>) {
    const chatEntity = await this.findChatById(chat.id);
    return this.messageRepository.find({ chat: chatEntity });
  }
}
