import { Test, TestingModule } from "@nestjs/testing";
import { ChatController } from "./chat.controller";
import { AppModule } from "../app.module";
import { TypeOrmModule } from "@nestjs/typeorm";
import { ChatMessage } from "../entities/chat.message.entity";
import { Chat } from "../entities/chat.entity";

describe("Chat Controller", () => {
  let controller: ChatController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule, TypeOrmModule.forFeature([Chat, ChatMessage])],
      controllers: [ChatController]
    }).compile();

    controller = module.get<ChatController>(ChatController);
  });

  it("should be defined", () => {
    expect(controller).toBeDefined();
  });
});
