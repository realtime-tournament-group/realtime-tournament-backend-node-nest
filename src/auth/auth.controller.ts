import { Body, Controller, Post } from "@nestjs/common";
import { UsersService } from "../users/users.service";
import { AuthService } from "./auth.service";
import { User } from "../entities/user.entity";

@Controller("auth")
export class AuthController {
  constructor(
    private readonly usersService: UsersService,
    private readonly authService: AuthService
  ) {}

  @Post()
  public register(@Body() user: Partial<User>) {
    this.usersService.createUser(user.name, user.password);
  }

  @Post("/login")
  public login(@Body() user: Partial<User>) {
    return this.authService.login(user);
  }
}
