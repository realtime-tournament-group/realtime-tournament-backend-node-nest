import { Injectable } from "@nestjs/common";
import { UsersService } from "../users/users.service";
import { JwtService } from "@nestjs/jwt";
import { Player } from "../entities/player.entity";

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService
  ) {}

  async validateUser(username: string, pass: string): Promise<boolean> {
    const user = await this.usersService.checkUser(username, pass);
    return user !== null;
  }

  async login(user: Partial<Player>) {
    const payload = { username: user.name, sub: user.id };
    return {
      access_token: this.jwtService.sign(payload)
    };
  }
}
