import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { SwaggerModule, DocumentBuilder } from "@nestjs/swagger";
import * as helmet from "helmet";
import { GatewayModule } from "./gateways/gateway.module";
declare const module: any;

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: console
  });
  app.enableCors({ origin: false });

  // app.use(helmet());
  const tour_options = new DocumentBuilder()
    .setTitle("Tournament service")
    .setDescription("The tournament API description")
    .setVersion("1.0")
    .addTag("tournament")
    .build();
  const document = SwaggerModule.createDocument(app, tour_options);
  SwaggerModule.setup("api", app, document);

  const gateOptions = new DocumentBuilder()
    .setTitle("Gateway options")
    .setDescription("The Gateway API description")
    .setVersion("1.0")
    .addTag("realtime_gateway")
    .build();

  const gates = SwaggerModule.createDocument(app, gateOptions, {
    include: [GatewayModule]
  });
  SwaggerModule.setup("api/realtime", app, gates);

  if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => app.close());
  }
  await app.listen(4000);
}

bootstrap();
