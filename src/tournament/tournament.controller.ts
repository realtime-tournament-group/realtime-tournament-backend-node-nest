import {
  Controller,
  Get,
  Post,
  Req,
  Res,
  Param,
  Body,
  Put,
  Delete,
  BadRequestException,
} from '@nestjs/common';
import { TournamentService } from './tournament.service';
import { Tournament } from '../entities/tournament.entity';
import { Player } from '../entities/player.entity';
import { PublicTournamentDto } from './dto/tournament';
import { UsersService } from '../users/users.service';
import { sha256 } from 'js-sha256';

@Controller('tournament')
export class TournamentController {
  constructor(private readonly tourService: TournamentService) {}

  @Get()
  async findAll() {
    return (await this.tourService.findAll()).map((item: Tournament) => ({
      id: item.id,
      name: item.name,
      status: item.status,
      open: item.open,
      matches: item.matches,
      players: item.players,
      creationDate: item.creationDate,
      gifts: item.gifts,
    }));
  }
  @Post('check')
  async checkHash(@Body() payload: { hash: string }): Promise<{
    status: boolean;
  }> {
    console.log(payload);
    const model = await this.tourService.findByPrivateHash(payload.hash);
    if (model) {
      return { status: model.privateHash === payload.hash };
    } else {
      return { status: false };
    }
  }
  @Post('connect')
  async connectTournament(@Body() payload: { name: string }) {
    console.log(payload);
    const model = await this.tourService.findByName(payload.name);
    if (model) {
      return true;
    } else {
      return new BadRequestException();
    }
  }
  @Get('find/:name')
  async findByName(@Param('name') name: string): Promise<PublicTournamentDto> {
    return this.tourService.findByName(name);
  }

  @Get(':id')
  async findOne(@Param('id') id: number): Promise<PublicTournamentDto> {
    return await this.tourService.findById(id);
  }

  @Post()
  async create(
    @Body() tournament: Partial<Tournament>,
  ): Promise<Partial<Tournament>> {
    console.log(tournament);
    return this.tourService.create(tournament);
  }

  @Put()
  async update(@Body() tournament: Partial<Tournament>): Promise<Tournament> {
    return this.tourService.update(tournament);
  }

  @Delete()
  async delete(@Body() tournament: Partial<Tournament>) {
    await this.tourService.delete(tournament);
  }
}
