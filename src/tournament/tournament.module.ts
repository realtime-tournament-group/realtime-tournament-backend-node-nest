import { Module } from "@nestjs/common";
import { TournamentController } from "./tournament.controller";
import { TournamentService } from "./tournament.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Tournament } from "../entities/tournament.entity";
import { TournamentGatewayService } from "../gateways/tournament/tournament.gateway.service";
import { TournamentGateway } from "../gateways/tournament/tournament.gateway";
import { TournamentGatewayController } from "../gateways/tournament/tournament.gateway.controller";
import { UsersModule } from "../users/users.module";
import { UsersService } from "../users/users.service";
import { JudgeService } from "../judge/judge.service";
import { JudgeModule } from "../judge/judge.module";

@Module({
  imports: [TypeOrmModule.forFeature([Tournament]), UsersModule],
  controllers: [TournamentController],
  providers: [TournamentService],
  exports: [TournamentService]
})
export class TournamentModule {}
