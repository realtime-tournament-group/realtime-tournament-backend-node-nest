import { Test, TestingModule } from "@nestjs/testing";
import { TournamentController } from "./tournament.controller";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Tournament } from "../entities/tournament.entity";
import { TournamentService } from "./tournament.service";
import { AppModule } from "../app.module";

describe("Tournament Controller", () => {
  let controller: TournamentController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule, TypeOrmModule.forFeature([Tournament])],

      providers: [TournamentService],
      controllers: [TournamentController]
    }).compile();

    controller = module.get<TournamentController>(TournamentController);
  });

  it("should be defined", () => {
    expect(controller).toBeDefined();
  });
});
