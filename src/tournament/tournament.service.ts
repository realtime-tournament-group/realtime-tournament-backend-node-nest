import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { sha256 } from 'js-sha256';
import { Tournament } from '../entities/tournament.entity';
import { Repository } from 'typeorm';
import { tournamentConfig } from './contants';

@Injectable()
export class TournamentService {
  constructor(
    @InjectRepository(Tournament)
    private readonly tourRepository: Repository<Tournament>,
  ) {}

  public async create(
    tournament: Partial<Tournament>,
  ): Promise<Partial<Tournament>> {
    const { name, open, status } = tournament;
    const hash = sha256(name + tournamentConfig);
    const privateHash = sha256(name + tournamentConfig + new Date().valueOf());
    const model = this.tourRepository.create({
      name,
      open,
      status,
      publicHash: hash,
      privateHash,
      creationDate: new Date(),
    });
    await this.tourRepository.save(model);
    return { name, privateHash };
  }

  public async update(tournament: Partial<Tournament>): Promise<Tournament> {
    await this.tourRepository.save({ id: tournament.id }, tournament);
    return this.tourRepository.findOne({
      where: { id: tournament.id },
      relations: ['players', 'matches'],
    });
  }

  public async findById(id: number): Promise<Tournament> {
    return this.tourRepository.findOne({ id });
  }

  public async findByName(name: string): Promise<Tournament> {
    return this.tourRepository.findOne({
      where: { name },
      relations: ['players', 'matches'],
      cache: 6000,
    });
  }
  public async findAll(): Promise<Tournament[]> {
    return this.tourRepository.find({ relations: ['players', 'matches'] });
  }
  public async findByPrivateHash(privateHash: string): Promise<Tournament> {
    return this.tourRepository.findOne({
      where: { privateHash },
      relations: ['players', 'matches'],
    });
  }
  public async findByPublicHash(publicHash: string): Promise<Tournament> {
    return this.tourRepository.findOne({
      where: { publicHash },
      relations: ['players', 'matches'],
    });
  }

  public async delete(tournament: Partial<Tournament>) {
    return this.tourRepository.delete(tournament);
  }
}
