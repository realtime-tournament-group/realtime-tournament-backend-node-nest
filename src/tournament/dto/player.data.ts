import { ApiModelProperty } from '@nestjs/swagger';

export class PlayerDataDto {
  @ApiModelProperty()
  name: string;
  @ApiModelProperty()
  tournamentName: string;
}
