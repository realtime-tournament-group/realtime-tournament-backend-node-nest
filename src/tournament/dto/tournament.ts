import { Match } from '../../entities/match.entity';

import { Player } from '../../entities/player.entity';

import { Gift } from '../../entities/gift.entity';

import { Judge } from '../../entities/judge.entity';

export interface PublicTournamentDto {
  id: number;

  name: string;
  publicHash: string;
  status: boolean;
  open: boolean;

  matches: Match[];
  players: Player[];
  creationDate: Date;

  gifts: Gift[];
  judges: Judge[];
}
