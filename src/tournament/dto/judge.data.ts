import { ApiModelProperty } from '@nestjs/swagger';
import { Tournament } from '../../entities/tournament.entity';
import { Player } from '../../entities/player.entity';

export class JudgeDataDto {
  @ApiModelProperty()
  hash: string;
  @ApiModelProperty()
  player: Partial<Player>;
  @ApiModelProperty()
  score: number;
}
