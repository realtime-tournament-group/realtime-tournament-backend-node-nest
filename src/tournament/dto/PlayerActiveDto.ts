import { ApiModelProperty } from '@nestjs/swagger';
export class PlayerActiveDto {
  @ApiModelProperty()
  hash: string;
  @ApiModelProperty()
  tournamentName: string;
}
