import { PrivateDataDto } from './private.data';
import { ApiModelProperty } from '@nestjs/swagger';
import { Player } from '../../entities/player.entity';
export class SetJudgeDto extends PrivateDataDto {
  @ApiModelProperty()
  hash: string;
  @ApiModelProperty()
  player: Partial<Player>;
}
