import { ApiModelProperty } from '@nestjs/swagger';

export class PrivateDataDto {
  @ApiModelProperty()
  privateHash: string;
}
