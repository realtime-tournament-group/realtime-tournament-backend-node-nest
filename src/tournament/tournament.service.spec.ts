import { Test, TestingModule } from "@nestjs/testing";
import { TournamentService } from "./tournament.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Tournament } from "../entities/tournament.entity";
import { TournamentController } from "./tournament.controller";
import { UsersController } from "../users/users.controller";
import { UsersService } from "../users/users.service";
import { AppModule } from "../app.module";

describe("TournamentService", () => {
  let service: TournamentService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule, TypeOrmModule.forFeature([Tournament])],
      controllers: [TournamentController],
      providers: [TournamentService]
    }).compile();

    service = module.get<TournamentService>(TournamentService);
  });

  it("should be defined", () => {
    expect(service).toBeDefined();
  });
});
