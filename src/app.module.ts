import { Module } from "@nestjs/common";
import { TournamentModule } from "./tournament/tournament.module";
import { AuthModule } from "./auth/auth.module";
import { UsersModule } from "./users/users.module";
import { TypeOrmModule } from "@nestjs/typeorm";

import { GatewayModule } from "./gateways/gateway.module";
import { JudgeModule } from "./judge/judge.module";
import { PlayerModule } from "./player/player.module";
import { MatchModule } from "./match/match.module";
import { ServeStaticModule } from "@nestjs/serve-static";
import { join } from "path";
@Module({
  imports: [
    TournamentModule,
    AuthModule,
    JudgeModule,
    GatewayModule,
    TypeOrmModule.forRootAsync({
      useFactory: () => ({
        type: "postgres",
        host: "db",
        port: 5432,
        username: "postgres",
        password: "postgres",
        database: "realtime",
        entities: [__dirname + "/**/*.entity{.ts,.js}"],
        synchronize: true
      })
    }),
    JudgeModule,
    PlayerModule,
    MatchModule,
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, "..", "client")
    })
  ],

  exports: [
    TypeOrmModule.forRootAsync({
      useFactory: () => ({
        type: "postgres",
        host: "db",
        port: 5432,
        username: "postgres",
        password: "postgres",
        database: "realtime",
        entities: [__dirname + "/**/*.entity{.ts,.js}"],
        synchronize: true
      })
    })
  ]
})
export class AppModule {}
