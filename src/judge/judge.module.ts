import { Module } from "@nestjs/common";
import { JudgeService } from "./judge.service";
import { Judge } from "../entities/judge.entity";
import { TypeOrmModule } from "@nestjs/typeorm";

@Module({
  imports: [TypeOrmModule.forFeature([Judge])],
  providers: [JudgeService],
  exports: [JudgeService]
})
export class JudgeModule {}
