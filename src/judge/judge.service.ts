import { Injectable } from '@nestjs/common';
import { Judge } from '../entities/judge.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Tournament } from '../entities/tournament.entity';
import { Player } from '../entities/player.entity';
import { sha256 } from 'js-sha256';

@Injectable()
export class JudgeService {
  constructor(
    @InjectRepository(Judge) private readonly judgeRepository: Repository<Judge>,
  ) {}

  public async findInTournament(tournament: Tournament, player: Player) {
    return this.judgeRepository.findOne({ tournament, player });
  }

  public async add(tournament: Tournament, player: Player) {
    this.judgeRepository.create({
      tournament,
      player,
      hash: sha256(new Date().toISOString() + 'judge_secret'),
    });
  }
  public async findByHash(hash: string) {
    return this.judgeRepository.findOne({ hash });
  }
  public async remove(tournament: Tournament, player: Player) {
    return this.judgeRepository.delete({ tournament, player });
  }
}
