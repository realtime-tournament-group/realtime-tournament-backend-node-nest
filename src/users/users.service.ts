import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../entities/user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
  ) {}

  async createUser(name: string, password: string): Promise<User> {
    const model = await this.usersRepository.create({ name, password });
    await this.usersRepository.save(model);
    return model;
  }

  async updateUser(user: Partial<User>) {
    await this.usersRepository.save(user);
  }

  async findUserById(id: number): Promise<User> {
    return await this.usersRepository.findOne({ id });
  }

  async removeUser(user: User) {
    await this.usersRepository.remove(user);
  }

  async findUserByName(name: string) {
    return this.usersRepository.findOne({ name });
  }

  async checkUser(name: string, password: string) {
    return await this.usersRepository.findOneOrFail({ name, password });
  }
}
