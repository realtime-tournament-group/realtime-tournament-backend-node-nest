import { Controller, Get, Param, Post, Body } from "@nestjs/common";
import { UsersService } from "./users.service";

@Controller("users")
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get(":id")
  public findUser(@Param("id") id: number) {
    return this.usersService.findUserById(id);
  }
}
