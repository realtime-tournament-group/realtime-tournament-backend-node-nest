import { MigrationInterface, QueryRunner } from "typeorm";

export class migrationNameHere1565613857527 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `CREATE TABLE "round" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, CONSTRAINT "PK_34bd959f3f4a90eb86e4ae24d2d" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TABLE "match" ("id" SERIAL NOT NULL, "status" boolean NOT NULL, CONSTRAINT "PK_92b6c3a6631dd5b24a67c69f69d" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TABLE "player" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "hash" character varying NOT NULL, "score" integer NOT NULL, CONSTRAINT "PK_65edadc946a7faf4b638d5e8885" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TABLE "gift" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "hash" character varying NOT NULL, CONSTRAINT "PK_f91217caddc01a085837ebe0606" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TABLE "judge" ("id" SERIAL NOT NULL, CONSTRAINT "PK_e686dcaea5ac575a0a7fded3b46" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TABLE "tournament" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "description" character varying(100) NOT NULL, "creationDate" TIMESTAMP NOT NULL, "hash" character varying NOT NULL, "chatId" integer, CONSTRAINT "REL_ba0c64ccfe977b484525a50099" UNIQUE ("chatId"), CONSTRAINT "PK_449f912ba2b62be003f0c22e767" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TABLE "user" ("id" SERIAL NOT NULL, CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TABLE "chat_message" ("id" SERIAL NOT NULL, "author" character varying NOT NULL, "message" character varying(500) NOT NULL, "chatId" integer, CONSTRAINT "REL_6d2db5b1118d92e561f5ebc1af" UNIQUE ("chatId"), CONSTRAINT "PK_3cc0d85193aade457d3077dd06b" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TABLE "chat" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, CONSTRAINT "PK_9d0b2ba74336710fd31154738a5" PRIMARY KEY ("id"))`
    );
    await queryRunner.query(
      `CREATE TABLE "player_matches_match" ("playerId" integer NOT NULL, "matchId" integer NOT NULL, CONSTRAINT "PK_8239ec2f8da9dae05dad1d0ceb3" PRIMARY KEY ("playerId", "matchId"))`
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_d30521dc1d8398bcb93542b377" ON "player_matches_match" ("playerId") `
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_1d15862e9aff34f10d2b826c2e" ON "player_matches_match" ("matchId") `
    );
    await queryRunner.query(
      `CREATE TABLE "tournament_users_user" ("tournamentId" integer NOT NULL, "userId" integer NOT NULL, CONSTRAINT "PK_dcfb9694ee74b245b4c1c2c1f9b" PRIMARY KEY ("tournamentId", "userId"))`
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_045d17f0b7df412256ea62b78a" ON "tournament_users_user" ("tournamentId") `
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_c415bf04c06e491681c2221289" ON "tournament_users_user" ("userId") `
    );
    await queryRunner.query(
      `CREATE TABLE "tournament_players_player" ("tournamentId" integer NOT NULL, "playerId" integer NOT NULL, CONSTRAINT "PK_94d8ba641e1bca3111f1d4e9fc1" PRIMARY KEY ("tournamentId", "playerId"))`
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_9453270aed1e0eb812a60d6b49" ON "tournament_players_player" ("tournamentId") `
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_db87d44b293849d97c543a9d52" ON "tournament_players_player" ("playerId") `
    );
    await queryRunner.query(
      `CREATE TABLE "chat_users_user" ("chatId" integer NOT NULL, "userId" integer NOT NULL, CONSTRAINT "PK_c6af481280fb886733ddbd73661" PRIMARY KEY ("chatId", "userId"))`
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_6a573fa22dfa3574496311588c" ON "chat_users_user" ("chatId") `
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_2004be39e2b3044c392bfe3e61" ON "chat_users_user" ("userId") `
    );
    await queryRunner.query(
      `CREATE TABLE "chat_players_player" ("chatId" integer NOT NULL, "playerId" integer NOT NULL, CONSTRAINT "PK_111b46eee2e3abcc707e4c33891" PRIMARY KEY ("chatId", "playerId"))`
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_4b28861a07b88c509cae309a5b" ON "chat_players_player" ("chatId") `
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_8a45393110e7249a23b032f818" ON "chat_players_player" ("playerId") `
    );
    await queryRunner.query(
      `ALTER TABLE "tournament" ADD CONSTRAINT "FK_ba0c64ccfe977b484525a500992" FOREIGN KEY ("chatId") REFERENCES "chat"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "chat_message" ADD CONSTRAINT "FK_6d2db5b1118d92e561f5ebc1af0" FOREIGN KEY ("chatId") REFERENCES "chat"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "player_matches_match" ADD CONSTRAINT "FK_d30521dc1d8398bcb93542b3776" FOREIGN KEY ("playerId") REFERENCES "player"("id") ON DELETE CASCADE ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "player_matches_match" ADD CONSTRAINT "FK_1d15862e9aff34f10d2b826c2e5" FOREIGN KEY ("matchId") REFERENCES "match"("id") ON DELETE CASCADE ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "tournament_users_user" ADD CONSTRAINT "FK_045d17f0b7df412256ea62b78a4" FOREIGN KEY ("tournamentId") REFERENCES "tournament"("id") ON DELETE CASCADE ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "tournament_users_user" ADD CONSTRAINT "FK_c415bf04c06e491681c22212894" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "tournament_players_player" ADD CONSTRAINT "FK_9453270aed1e0eb812a60d6b490" FOREIGN KEY ("tournamentId") REFERENCES "tournament"("id") ON DELETE CASCADE ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "tournament_players_player" ADD CONSTRAINT "FK_db87d44b293849d97c543a9d52c" FOREIGN KEY ("playerId") REFERENCES "player"("id") ON DELETE CASCADE ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "chat_users_user" ADD CONSTRAINT "FK_6a573fa22dfa3574496311588c7" FOREIGN KEY ("chatId") REFERENCES "chat"("id") ON DELETE CASCADE ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "chat_users_user" ADD CONSTRAINT "FK_2004be39e2b3044c392bfe3e617" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE CASCADE ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "chat_players_player" ADD CONSTRAINT "FK_4b28861a07b88c509cae309a5b9" FOREIGN KEY ("chatId") REFERENCES "chat"("id") ON DELETE CASCADE ON UPDATE NO ACTION`
    );
    await queryRunner.query(
      `ALTER TABLE "chat_players_player" ADD CONSTRAINT "FK_8a45393110e7249a23b032f8189" FOREIGN KEY ("playerId") REFERENCES "player"("id") ON DELETE CASCADE ON UPDATE NO ACTION`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "chat_players_player" DROP CONSTRAINT "FK_8a45393110e7249a23b032f8189"`
    );
    await queryRunner.query(
      `ALTER TABLE "chat_players_player" DROP CONSTRAINT "FK_4b28861a07b88c509cae309a5b9"`
    );
    await queryRunner.query(
      `ALTER TABLE "chat_users_user" DROP CONSTRAINT "FK_2004be39e2b3044c392bfe3e617"`
    );
    await queryRunner.query(
      `ALTER TABLE "chat_users_user" DROP CONSTRAINT "FK_6a573fa22dfa3574496311588c7"`
    );
    await queryRunner.query(
      `ALTER TABLE "tournament_players_player" DROP CONSTRAINT "FK_db87d44b293849d97c543a9d52c"`
    );
    await queryRunner.query(
      `ALTER TABLE "tournament_players_player" DROP CONSTRAINT "FK_9453270aed1e0eb812a60d6b490"`
    );
    await queryRunner.query(
      `ALTER TABLE "tournament_users_user" DROP CONSTRAINT "FK_c415bf04c06e491681c22212894"`
    );
    await queryRunner.query(
      `ALTER TABLE "tournament_users_user" DROP CONSTRAINT "FK_045d17f0b7df412256ea62b78a4"`
    );
    await queryRunner.query(
      `ALTER TABLE "player_matches_match" DROP CONSTRAINT "FK_1d15862e9aff34f10d2b826c2e5"`
    );
    await queryRunner.query(
      `ALTER TABLE "player_matches_match" DROP CONSTRAINT "FK_d30521dc1d8398bcb93542b3776"`
    );
    await queryRunner.query(
      `ALTER TABLE "chat_message" DROP CONSTRAINT "FK_6d2db5b1118d92e561f5ebc1af0"`
    );
    await queryRunner.query(
      `ALTER TABLE "tournament" DROP CONSTRAINT "FK_ba0c64ccfe977b484525a500992"`
    );
    await queryRunner.query(`DROP INDEX "IDX_8a45393110e7249a23b032f818"`);
    await queryRunner.query(`DROP INDEX "IDX_4b28861a07b88c509cae309a5b"`);
    await queryRunner.query(`DROP TABLE "chat_players_player"`);
    await queryRunner.query(`DROP INDEX "IDX_2004be39e2b3044c392bfe3e61"`);
    await queryRunner.query(`DROP INDEX "IDX_6a573fa22dfa3574496311588c"`);
    await queryRunner.query(`DROP TABLE "chat_users_user"`);
    await queryRunner.query(`DROP INDEX "IDX_db87d44b293849d97c543a9d52"`);
    await queryRunner.query(`DROP INDEX "IDX_9453270aed1e0eb812a60d6b49"`);
    await queryRunner.query(`DROP TABLE "tournament_players_player"`);
    await queryRunner.query(`DROP INDEX "IDX_c415bf04c06e491681c2221289"`);
    await queryRunner.query(`DROP INDEX "IDX_045d17f0b7df412256ea62b78a"`);
    await queryRunner.query(`DROP TABLE "tournament_users_user"`);
    await queryRunner.query(`DROP INDEX "IDX_1d15862e9aff34f10d2b826c2e"`);
    await queryRunner.query(`DROP INDEX "IDX_d30521dc1d8398bcb93542b377"`);
    await queryRunner.query(`DROP TABLE "player_matches_match"`);
    await queryRunner.query(`DROP TABLE "chat"`);
    await queryRunner.query(`DROP TABLE "chat_message"`);
    await queryRunner.query(`DROP TABLE "user"`);
    await queryRunner.query(`DROP TABLE "tournament"`);
    await queryRunner.query(`DROP TABLE "judge"`);
    await queryRunner.query(`DROP TABLE "gift"`);
    await queryRunner.query(`DROP TABLE "player"`);
    await queryRunner.query(`DROP TABLE "match"`);
    await queryRunner.query(`DROP TABLE "round"`);
  }
}
