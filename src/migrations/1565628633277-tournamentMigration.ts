import { MigrationInterface, QueryRunner } from "typeorm";

export class tournamentMigration1565628633277 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "tournament" ALTER COLUMN "creationDate" DROP NOT NULL`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "tournament" ALTER COLUMN "creationDate" SET NOT NULL`
    );
  }
}
